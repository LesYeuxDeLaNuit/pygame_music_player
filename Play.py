import pygame
import random
import os

list = os.listdir("musique")

def pmusic(file):
    pygame.mixer.init()
    clock = pygame.time.Clock()
    pygame.mixer.music.load(file)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        print("Lecture de " + file)
        input("Press touch...")
        print("Fin de " + file)
        print("")
        stopmusic()
        start()

def stopmusic():
    pygame.mixer.music.stop()


def getmixerargs():
    pygame.mixer.init()
    freq, size, chan = pygame.mixer.get_init()
    return freq, size, chan


def initMixer():
    BUFFER = 3072
    FREQ, SIZE, CHAN = getmixerargs()
    pygame.mixer.init(FREQ, SIZE, CHAN, BUFFER)


def start():
    file = random.choice(list)
    file = "musique/" + file
    pmusic(file)

try:
    initMixer()
    start()

except KeyboardInterrupt:
    stopmusic()
    print("\nPlay Stopped by user")
except Exception:
    print("unknown error")

print("Done")

